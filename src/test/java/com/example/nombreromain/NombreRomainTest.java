package com.example.nombreromain;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class NombreRomainTest {
    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 1" +
                    "QUAND on le transforme en chiffre Romain" +
                    "ALORS on obtient I ")
    public void Test_Chiffre_Arabe_1_En_Chiffre_Romain_I() {
        // ETANT donné un chiffre Arabe 1
        int chiffreArabeInitial = 1;
        String chiffreRomainInitial = "I";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomaine(chiffreArabeInitial);

        // ALORS on obtient I
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 2" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient II")
    public void Test_Chiffre_Arabe_2_En_Chiffre_Romain_II() {
        // ETANT donné un chiffre Arabe 2
        int chiffreArabeInitial = 2;
        String chiffreRomainInitial = "II";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomaine(chiffreArabeInitial);

        // ALORS on obtient II
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 3" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient III")
    public void Test_Chiffre_Arabe_3_En_Chiffre_Romain_III() {
        // ETANT donné un chiffre Arabe 3
        int chiffreArabeInitial = 3;
        String chiffreRomainInitial = "III";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomaine(chiffreArabeInitial);

        // ALORS on obtient III
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 4" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient IV")
    public void Test_Chiffre_Arabe_4_En_Chiffre_Romain_IV() {
        // ETANT donné un chiffre Arabe 1
        int chiffreArabeInitial = 4;
        String chiffreRomainInitial = "IV";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomaine(chiffreArabeInitial);

        // ALORS on obtient IV
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 5" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient V")
    public void Test_Chiffre_Arabe_5_En_Chiffre_Romain_V() {
        // ETANT donné un chiffre Arabe 5
        int chiffreArabeInitial = 5;
        String chiffreRomainInitial = "V";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomaine(chiffreArabeInitial);

        // ALORS on obtient V
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 6" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient VI")
    public void Test_Chiffre_Arabe_6_En_Chiffre_Romain_VI() {
        // ETANT donné un chiffre Arabe 6
        int chiffreArabeInitial = 6;
        String chiffreRomainInitial = "VI";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomaine(chiffreArabeInitial);

        // ALORS on obtient VI
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 7" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient VII")
    public void Test_Chiffre_Arabe_7_En_Chiffre_Romain_VII() {
        // ETANT donné un chiffre Arabe 7
        int chiffreArabeInitial = 7;
        String chiffreRomainInitial = "VII";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomaine(chiffreArabeInitial);

        // ALORS on obtient VII
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 8" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient VIII")
    public void Test_Chiffre_Arabe_8_En_Chiffre_Romain_VIII() {
        // ETANT donné un chiffre Arabe 1
        int chiffreArabeInitial = 1;
        String chiffreRomainInitial = "I";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomaine(chiffreArabeInitial);

        // ALORS on obtient I
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 9" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient IX")
    public void Test_Chiffre_Arabe_9_En_Chiffre_Romain_X() {
        // ETANT donné un chiffre Arabe 9
        int chiffreArabeInitial = 9;
        String chiffreRomainInitial = "IX";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomaine(chiffreArabeInitial);

        // ALORS on obtient IX
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 10" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient X")
    public void Test_Chiffre_Arabe_10_En_Chiffre_Romain_X() {
        // ETANT donné un chiffre Arabe 10
        int chiffreArabeInitial = 10;
        String chiffreRomainInitial = "X";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomaine(chiffreArabeInitial);

        // ALORS on obtient X
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 11" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XI")
    public void Test_Chiffre_Arabe_11_En_Chiffre_Romain_XI() {
        // ETANT donné un chiffre Arabe 1
        int chiffreArabeInitial = 11;
        String chiffreRomainInitial = "XI";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomaine(chiffreArabeInitial);

        // ALORS on obtient XI
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 12" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XII")
    public void Test_Chiffre_Arabe_12_En_Chiffre_Romain_XII() {
        // ETANT donné un chiffre Arabe 12
        int chiffreArabeInitial = 12;
        String chiffreRomainInitial = "XII";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomaine(chiffreArabeInitial);

        // ALORS on obtient XII
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 13" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XIII")
    public void Test_Chiffre_Arabe_13_En_Chiffre_Romain_XIII() {
        // ETANT donné un chiffre Arabe 13
        int chiffreArabeInitial = 13;
        String chiffreRomainInitial = "XIII";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XIII
        assertEquals(chiffreRomainInitial, chiffreRomainFinal);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 14" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XIV")
    public void Test_Chiffre_Arabe_14_En_Chiffre_Romain_XIV() {
        // ETANT donné un chiffre Arabe 14
        int chiffreArabeInitial = 14;
        String chiffreRomainInitial = "XIV";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XIV
        assertEquals(chiffreRomainInitial, chiffreRomainFinal);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 15" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XV")
    public void Test_Chiffre_Arabe_15_En_Chiffre_Romain_XV() {
        // ETANT donné un chiffre Arabe 15
        int chiffreArabeInitial = 15;
        String chiffreRomainInitial = "XV";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XV
        assertEquals(chiffreRomainInitial, chiffreRomainFinal);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 16" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XVI")
    public void Test_Chiffre_Arabe_16_En_Chiffre_Romain_XVI() {
        // ETANT donné un chiffre Arabe 16
        int chiffreArabeInitial = 16;
        String chiffreRomainInitial = "XVI";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XVI
        assertEquals(chiffreRomainInitial, chiffreRomainFinal);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 17" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XVII")
    public void Test_Chiffre_Arabe_17_En_Chiffre_Romain_XVII() {
        // ETANT donné un chiffre Arabe 17
        int chiffreArabeInitial = 17;
        String chiffreRomainInitial = "XVII";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XVII
        assertEquals(chiffreRomainInitial, chiffreRomainFinal);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 18" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XVIII")
    public void Test_Chiffre_Arabe_18_En_Chiffre_Romain_XVIII() {
        // ETANT donné un chiffre Arabe 18
        int chiffreArabeInitial = 18;
        String chiffreRomainInitial = "XVIII";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XVIII
        assertEquals(chiffreRomainInitial, chiffreRomainFinal);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 20" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XX")
    public void Test_Chiffre_Arabe_20_En_Chiffre_Romain_XX() {
        // ETANT donné un chiffre Arabe 20
        int chiffreArabeInitial = 20;
        String chiffreRomainInitial = "XX";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XX
        assertEquals(chiffreRomainInitial, chiffreRomainFinal);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 19" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XIX")
    public void Test_Chiffre_Arabe_19_En_Chiffre_Romain_XIX() {
        // ETANT donné un chiffre Arabe 19
        int chiffreArabeInitial = 19;
        String chiffreRomainInitial = "XIX";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XIX
        assertEquals(chiffreRomainInitial, chiffreRomainFinal);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 21" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XXI")
    public void Test_Chiffre_Arabe_21_En_Chiffre_Romain_XXI() {
        // ETANT donné un chiffre Arabe 21
        int chiffreArabeInitial = 21;
        String chiffreRomainInitial = "XXI";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XXI
        assertEquals(chiffreRomainInitial, chiffreRomainFinal);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 22" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XXII")
    public void Test_Chiffre_Arabe_22_En_Chiffre_Romain_XXII() {
        // ETANT donné un chiffre Arabe 22
        int chiffreArabeInitial = 22;
        String chiffreRomainInitial = "XXII";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XXII
        assertEquals(chiffreRomainInitial, chiffreRomainFinal);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 23" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XXIII")
    public void Test_Chiffre_Arabe_23_En_Chiffre_Romain_XXIII() {
        // ETANT donné un chiffre Arabe 23
        int chiffreArabeInitial = 23;
        String chiffreRomainInitial = "XXIII";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XXIII
        assertEquals(chiffreRomainInitial, chiffreRomainFinal);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 24" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XXIV")
    public void Test_Chiffre_Arabe_24_En_Chiffre_Romain_XXIV() {
        // ETANT donné un chiffre Arabe 24
        int chiffreArabeInitial = 24;
        String chiffreRomainInitial = "XXIV";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XXIV
        assertEquals(chiffreRomainInitial, chiffreRomainFinal);
    }


    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 25" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XXV")
    public void Test_Chiffre_Arabe_25_En_Chiffre_Romain_XXV() {
        // ETANT donné un chiffre Arabe 25
        int chiffreArabeInitial = 25;
        String chiffreRomainInitial = "XXV";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XXV
        assertEquals(chiffreRomainInitial, chiffreRomainFinal);
    }


    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 26" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XXVI")
    public void Test_Chiffre_Arabe_26_En_Chiffre_Romain_XXVI() {
        // ETANT donné un chiffre Arabe 26
        int chiffreArabeInitial = 26;
        String chiffreRomainInitial = "XXVI";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XXVI
        assertEquals(chiffreRomainInitial, chiffreRomainFinal);
    }


    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 27" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XXVII")
    public void Test_Chiffre_Arabe_27_En_Chiffre_Romain_XXVII() {
        // ETANT donné un chiffre Arabe 27
        int chiffreArabeInitial = 27;
        String chiffreRomainInitial = "XXVII";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XXVII
        assertEquals(chiffreRomainInitial, chiffreRomainFinal);
    }


    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 28" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XXVIII")
        public void Test_Chiffre_Arabe_28_En_Chiffre_Romain_XXVIII() {
        // ETANT donné un chiffre Arabe 28
        int chiffreArabeInitial = 28;
        String chiffreRomainInitial = "XXVIII";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XXVIII
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }


    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 29" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XXIX")
    public void Test_Chiffre_Arabe_29_En_Chiffre_Romain_XXIX() {
        // ETANT donné un chiffre Arabe 29
        int chiffreArabeInitial = 29;
        String chiffreRomainInitial = "XXIX";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XXIX
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }


    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 30" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XXX")
    public void Test_Chiffre_Arabe_30_En_Chiffre_Romain_XXX() {
        // ETANT donné un chiffre Arabe 30
        int chiffreArabeInitial = 30;
        String chiffreRomainInitial = "XXX";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XXX
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }


    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 31" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XXXI")
    public void Test_Chiffre_Arabe_31_En_Chiffre_Romain_XXXI() {
        // ETANT donné un chiffre Arabe 31
        int chiffreArabeInitial = 31;
        String chiffreRomainInitial = "XXXI";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XXXI
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }


    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 32" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XXXII")
    public void Test_Chiffre_Arabe_32_En_Chiffre_Romain_XXXII() {
        // ETANT donné un chiffre Arabe 32
        int chiffreArabeInitial = 32;
        String chiffreRomainInitial = "XXXII";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XXXII
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 33" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XXXIII")
    public void Test_Chiffre_Arabe_33_En_Chiffre_Romain_XXXIII() {
        // ETANT donné un chiffre Arabe 33
        int chiffreArabeInitial = 33;
        String chiffreRomainInitial = "XXXIII";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XXXIII
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 34" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XXXIV")
    public void Test_Chiffre_Arabe_34_En_Chiffre_Romain_XXXIV() {
        // ETANT donné un chiffre Arabe 34
        int chiffreArabeInitial = 34;
        String chiffreRomainInitial = "XXXIV";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XXXIV
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 35" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XXXV")
    public void Test_Chiffre_Arabe_35_En_Chiffre_Romain_XXXV() {
        // ETANT donné un chiffre Arabe 35
        int chiffreArabeInitial = 35;
        String chiffreRomainInitial = "XXXV";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XXXV
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 36" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XXXVI")
    public void Test_Chiffre_Arabe_36_En_Chiffre_Romain_XXXVI() {
        // ETANT donné un chiffre Arabe 36
        int chiffreArabeInitial = 36;
        String chiffreRomainInitial = "XXXVI";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XXXVI
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 37" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XXXVII")
    public void Test_Chiffre_Arabe_37_En_Chiffre_Romain_XXXVII() {
        // ETANT donné un chiffre Arabe 37
        int chiffreArabeInitial = 37;
        String chiffreRomainInitial = "XXXVII";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XXXVII
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 38" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XXXVIII")
    public void Test_Chiffre_Arabe_38_En_Chiffre_Romain_XXXVIII() {
        // ETANT donné un chiffre Arabe 38
        int chiffreArabeInitial = 38;
        String chiffreRomainInitial = "XXXVIII";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XXXVIII
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 39" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XXXIX")
    public void Test_Chiffre_Arabe_39_En_Chiffre_Romain_XXXIX() {
        // ETANT donné un chiffre Arabe 39
        int chiffreArabeInitial = 39;
        String chiffreRomainInitial = "XXXIX";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XXXIX
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 40" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XL")
    public void Test_Chiffre_Arabe_40_En_Chiffre_Romain_XL() {
        // ETANT donné un chiffre Arabe 40
        int chiffreArabeInitial = 40;
        String chiffreRomainInitial = "XL";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XL
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 41" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XLI")
    public void Test_Chiffre_Arabe_41_En_Chiffre_Romain_XLI() {
        // ETANT donné un chiffre Arabe 41
        int chiffreArabeInitial = 41;
        String chiffreRomainInitial = "XLI";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XLI
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 42" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XLII")
    public void Test_Chiffre_Arabe_42_En_Chiffre_Romain_XLII() {
        // ETANT donné un chiffre Arabe 42
        int chiffreArabeInitial = 42;
        String chiffreRomainInitial = "XLII";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomainAmeliore(chiffreArabeInitial);

        // ALORS on obtient XLII
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 43" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XLIII")
    public void Test_Chiffre_Arabe_43_En_Chiffre_Romain_XLIII() {
        // ETANT donné un chiffre Arabe 43
        int chiffreArabeInitial = 43;
        String chiffreRomainInitial = "XLIII";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomaine(chiffreArabeInitial);

        // ALORS on obtient XLIII
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }

    @Test
    @DisplayName(   "ETANT donné un chiffre Arabe 44" +
            "QUAND on le transforme en chiffre Romain" +
            "ALORS on obtient XLIV")
    public void Test_Chiffre_Arabe_44_En_Chiffre_Romain_XLIV() {
        // ETANT donné un chiffre Arabe 44
        int chiffreArabeInitial = 44;
        String chiffreRomainInitial = "XLIV";

        // QUAND on le transforme en chiffre Romain
        String chiffreRomainFinal = NombreRomain.convertirChiffreArabeEnChiffreRomaine(chiffreArabeInitial);

        // ALORS on obtient XLIV
        assertEquals(chiffreRomainFinal, chiffreRomainInitial);
    }



}
